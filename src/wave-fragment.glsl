precision mediump float;

varying vec2 vUv;

void main() {
   float y = vUv.y;
   float invertY = 1.0 - y;
   float invertSqr = 1.0 - invertY * invertY;
   gl_FragColor = vec4( invertSqr, y - .8, .7 - invertSqr * .25, 1.0 );
}
