import { Group } from 'three'
import BasicLights from './lights'
import Wave from './wave'
import Turbo from './turbo'

export default class SeedScene extends Group {
  constructor() {
    super()

    this.waves = []

    for (let i = 0; i < 100; i++) {
      const wave = new Wave({ freq: 2 + 4 *  Math.random(), speed: .0005 + .0005 * (1 -  Math.random()) })
      const leftRight = Math.sign(i % 2 - .5)
      wave.position.x = 1.2 * leftRight + ( Math.random() - .5)
      wave.position.y = 0
      wave.position.z = .1 * i
      this.waves.push( wave )
    }

    this.lights = new BasicLights()

    this.add(...this.waves, this.lights)

    this.turbo = new Turbo()

    const input = document.createElement('input')
    document.body.appendChild(input)
    input.className = 'flame-input'
    input.placeholder = 'Type as fast as you can'

    const events = ['keydown', 'cut', 'paste']
    events.forEach((key) => {
      input.addEventListener(key, () => {
        this.turbo.flame()
      })
    })
  }

  update(timeStamp) {
    this.turbo.animate()
    this.waves.forEach((wave) => {
      wave.turbo = this.turbo.value
      wave.animate(timeStamp)
    })
  }
}
