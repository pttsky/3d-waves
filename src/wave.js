import * as THREE from 'three'
import SimplexNoise from 'simplex-noise'
import fragment from './wave-fragment.glsl'
import vertex from './wave-vertex.glsl'

const defaults = {
  verticeCount: 100,
  idle: .5,
  amplitude: .25,
  freq: 1,
  speed: .0005
}

const noise = new SimplexNoise()

export default class Wave extends THREE.Group {
  constructor(config = {}) {
    super()
    Object.assign( this, defaults, config )
    this.turbo = 1

    const bufferGeometry = new THREE.BufferGeometry()
    const vertices = []
    const indices = []
    for (let i = 0; i < this.verticeCount; i += 2) {
      const x = 2 * i / this.verticeCount - 1
      const y = this.value( x, 0 )
      vertices.push( x, 0, 0 )
      vertices.push( x, y, 0 )
      if (i > 0) {
        indices.push( i - 2, i, i + 1 )
        indices.push( i - 2, i + 1, i - 1 )
      }
    }
    bufferGeometry.setIndex( indices )
    bufferGeometry.addAttribute( 'position', new THREE.Float32BufferAttribute( vertices, 3 ) )
    const geometry = new THREE.Geometry().fromBufferGeometry( bufferGeometry )

    const material = new THREE.ShaderMaterial({ fragmentShader: fragment, vertexShader: vertex })

    const mesh = new THREE.Mesh( geometry, material )

    this.add(mesh)
    this.geometry = geometry
  }

  value(x, t) {
    const freq = (1 + .25 * (this.turbo - 1)) * this.freq
    const y = this.turbo * (this.idle + this.amplitude * noise.noise3D( .4 * freq * x, t, freq ))
    return (1 - Math.abs(x ** 3)) * y
  }

  animate(t) {
    this.geometry.vertices.forEach((vertice, i) => {
      if (i % 2 === 1) {
        vertice.y = this.value( vertice.x, this.speed * t )
      }
    })
    this.geometry.verticesNeedUpdate = true
  }
}
